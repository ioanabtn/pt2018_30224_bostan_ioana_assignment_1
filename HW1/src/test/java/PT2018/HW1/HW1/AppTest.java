package PT2018.HW1.HW1;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;

import junit.framework.Assert;
import model.MonomDouble;
import model.Polinom;

import static org.junit.Assert.assertArrayEquals;


public class AppTest {

	public String toString1(int c, int p) {
		return "+" + "(" + (int) (c) + "*x^" + p + ")";
	}

	DecimalFormat numberFormat = new DecimalFormat("0.00");
	
	public String toString2(double c, int p) {
		return "+" + "(" + numberFormat.format(c) + "*x^" + p + ")";
	}
	
	@Test
	public void Derivare() {
		Polinom p1 = new Polinom();
		
		Polinom rezultatulMeu = new Polinom();
		Polinom rezultatulMetodei = new Polinom();
		
		ArrayList<MonomDouble> l1 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l2 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l3 = new ArrayList<MonomDouble>();
		
		
		MonomDouble m11 = new MonomDouble(2,3);
		MonomDouble m12 = new MonomDouble(3,2);
		MonomDouble m13 = new MonomDouble(2,1);
		MonomDouble m14 = new MonomDouble(1,0);
		
		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		l1.add(m14);
	
		p1.setP(l1);
		
		
		
		MonomDouble m31 = new MonomDouble(6,2);
		MonomDouble m32 = new MonomDouble(6,1);
		MonomDouble m33 = new MonomDouble(2,0);
		
		
		l3.add(m31);
		l3.add(m32);
		l3.add(m33);
		
		
		rezultatulMeu.setP(l3);
		
		p1.Derivare();
		
		String rezultatulMeuS = " ";
		
		for (MonomDouble temp : rezultatulMeu.getP1()) {
			rezultatulMeuS = rezultatulMeuS + toString1((int) temp.getCoefficient(), temp.getPower());
		}
		
		String rezultatulMetodeiS = " ";
		
		for (MonomDouble temp : p1.getP1()) {
			rezultatulMetodeiS = rezultatulMetodeiS + toString1((int) temp.getCoefficient(), temp.getPower());
		}
		
		
		Assert.assertEquals(rezultatulMetodeiS, rezultatulMeuS);;		
	
	}
	
	@Test
	public void Integrare() {
		Polinom p1 = new Polinom();
		
		Polinom rezultatulMeu = new Polinom();
		Polinom rezultatulMetodei = new Polinom();
		
		ArrayList<MonomDouble> l1 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l2 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l3 = new ArrayList<MonomDouble>();
		
		
		MonomDouble m11 = new MonomDouble(2,3);
		MonomDouble m12 = new MonomDouble(3,2);
		MonomDouble m13 = new MonomDouble(2,1);
		MonomDouble m14 = new MonomDouble(1,0);
		
		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		l1.add(m14);
	
		p1.setP(l1);
		
	
		
		MonomDouble m31 = new MonomDouble(0.50,4);
		MonomDouble m32 = new MonomDouble(1.00,3);
		MonomDouble m33 = new MonomDouble(1.00,2);
		MonomDouble m34 = new MonomDouble(1.00,1);
		
		l3.add(m31);
		l3.add(m32);
		l3.add(m33);
		l3.add(m34);
		
		rezultatulMeu.setP(l3);
		
		p1.Integrare();
		
		String rezultatulMeuS = " ";
		
		for (MonomDouble temp : rezultatulMeu.getP1()) {
			rezultatulMeuS = rezultatulMeuS + toString2(temp.getCoefficient(), temp.getPower());
		}
		
		String rezultatulMetodeiS = " ";
		
		for (MonomDouble temp : p1.getP1()) {
			rezultatulMetodeiS = rezultatulMetodeiS + toString2(temp.getCoefficient(), temp.getPower());
		}
		
		
		
		Assert.assertEquals(rezultatulMetodeiS, rezultatulMeuS);		

	
		
	}
	
	@Test
	public void Adunare() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Polinom rezultatulMeu = new Polinom();
		Polinom rezultatulMetodei = new Polinom();

		ArrayList<MonomDouble> l1 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l2 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l3 = new ArrayList<MonomDouble>();

		MonomDouble m11 = new MonomDouble(2, 3);
		MonomDouble m12 = new MonomDouble(3, 2);
		MonomDouble m13 = new MonomDouble(2, 1);
		MonomDouble m14 = new MonomDouble(1, 0);

		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		l1.add(m14);

		p1.setP(l1);

		MonomDouble m21 = new MonomDouble(5, 2);
		MonomDouble m22 = new MonomDouble(6, 1);
		MonomDouble m23 = new MonomDouble(2, 0);

		l2.add(m21);
		l2.add(m22);
		l2.add(m23);

		p2.setP(l2);

		MonomDouble m31 = new MonomDouble(2, 3);
		MonomDouble m32 = new MonomDouble(8, 2);
		MonomDouble m33 = new MonomDouble(8, 1);
		MonomDouble m34 = new MonomDouble(3, 0);

		l3.add(m31);
		l3.add(m32);
		l3.add(m33);
		l3.add(m34);

		rezultatulMeu.setP(l3);

		rezultatulMetodei = p1.Adunare(p2);

		String rezultatulMeuS = " ";

		for (MonomDouble temp : rezultatulMeu.getP1()) {
			rezultatulMeuS = rezultatulMeuS + toString1((int) temp.getCoefficient(), temp.getPower());
		}

		String rezultatulMetodeiS = " ";

		for (MonomDouble temp : rezultatulMetodei.getP1()) {
			rezultatulMetodeiS = rezultatulMetodeiS + toString1((int) temp.getCoefficient(), temp.getPower());
		}

		Assert.assertEquals(rezultatulMetodeiS, rezultatulMeuS);

	}

	@Test
	public void Scadere() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Polinom rezultatulMeu = new Polinom();
		Polinom rezultatulMetodei = new Polinom();

		ArrayList<MonomDouble> l1 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l2 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l3 = new ArrayList<MonomDouble>();

		MonomDouble m11 = new MonomDouble(2, 3);
		MonomDouble m12 = new MonomDouble(3, 2);
		MonomDouble m13 = new MonomDouble(2, 1);
		MonomDouble m14 = new MonomDouble(1, 0);

		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		l1.add(m14);

		p1.setP(l1);

		MonomDouble m21 = new MonomDouble(5, 2);
		MonomDouble m22 = new MonomDouble(6, 1);
		MonomDouble m23 = new MonomDouble(2, 0);

		l2.add(m21);
		l2.add(m22);
		l2.add(m23);

		p2.setP(l2);

		MonomDouble m31 = new MonomDouble(2, 3);
		MonomDouble m32 = new MonomDouble(-2, 2);
		MonomDouble m33 = new MonomDouble(-4, 1);
		MonomDouble m34 = new MonomDouble(-1, 0);

		l3.add(m31);
		l3.add(m32);
		l3.add(m33);
		l3.add(m34);

		rezultatulMeu.setP(l3);

		rezultatulMetodei = p1.Scadere(p2);

		String rezultatulMeuS = " ";

		for (MonomDouble temp : rezultatulMeu.getP1()) {
			rezultatulMeuS = rezultatulMeuS + toString1((int) temp.getCoefficient(), temp.getPower());
		}

		String rezultatulMetodeiS = " ";

		for (MonomDouble temp : rezultatulMetodei.getP1()) {
			rezultatulMetodeiS = rezultatulMetodeiS + toString1((int) temp.getCoefficient(), temp.getPower());
		}

		Assert.assertEquals(rezultatulMetodeiS, rezultatulMeuS);

	}

	@Test
	public void Inmultire() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Polinom rezultatulMeu = new Polinom();
		Polinom rezultatulMetodei = new Polinom();

		ArrayList<MonomDouble> l1 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l2 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l3 = new ArrayList<MonomDouble>();

		MonomDouble m11 = new MonomDouble(2, 3);
		MonomDouble m12 = new MonomDouble(3, 2);
		MonomDouble m13 = new MonomDouble(2, 1);
		

		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		

		p1.setP(l1);

		MonomDouble m21 = new MonomDouble(5, 2);
		MonomDouble m22 = new MonomDouble(6, 1);
	

		l2.add(m21);
		l2.add(m22);
	

		p2.setP(l2);

		MonomDouble m31 = new MonomDouble(10,5);
		MonomDouble m32 = new MonomDouble(27, 4);
		MonomDouble m33 = new MonomDouble(28, 3);
		MonomDouble m34 = new MonomDouble(12, 2);

		l3.add(m31);
		l3.add(m32);
		l3.add(m33);
		l3.add(m34);

		rezultatulMeu.setP(l3);

		rezultatulMetodei.Inmultire(p1.getP1(), p2.getP1());

		String rezultatulMeuS = " ";

		for (MonomDouble temp : rezultatulMeu.getP1()) {
			rezultatulMeuS = rezultatulMeuS + toString1((int) temp.getCoefficient(), temp.getPower());
		}

		String rezultatulMetodeiS = " ";

		for (MonomDouble temp : rezultatulMetodei.getP1()) {
			rezultatulMetodeiS = rezultatulMetodeiS + toString1((int) temp.getCoefficient(), temp.getPower());
		}
		
		
		Assert.assertEquals(rezultatulMetodeiS, rezultatulMeuS);
		

	}

	@Test
	public void Impartire() {
		Polinom D = new Polinom();
		Polinom I = new Polinom();
		Polinom C = new Polinom();
		Polinom rest = new Polinom();
		Polinom restulMeu = new Polinom();
		Polinom rezultatulMeu = new Polinom();
		

		ArrayList<MonomDouble> l1 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l2 = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> l3 = new ArrayList<MonomDouble>();

		MonomDouble m11 = new MonomDouble(2, 3);
		MonomDouble m12 = new MonomDouble(3, 2);
		MonomDouble m13 = new MonomDouble(2, 1);
		

		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		

		D.setP(l1);

		MonomDouble m21 = new MonomDouble(5, 2);
		MonomDouble m22 = new MonomDouble(6, 1);
	

		l2.add(m21);
		l2.add(m22);
	

		I.setP(l2);

		MonomDouble m31 = new MonomDouble(0.40,1);
		MonomDouble m32 = new MonomDouble(0.12, 0);
		

		l3.add(m31);
		l3.add(m32);
		
		
		rezultatulMeu.setP(l3);

		C=D.Impartire(I, rest);

		MonomDouble r1 = new MonomDouble(1.28,1);
		
		restulMeu.getP1().add(r1);

		String rezultatulMeuS = " ";

		for (MonomDouble temp : rezultatulMeu.getP1()) {
			rezultatulMeuS = rezultatulMeuS + toString2( temp.getCoefficient(), temp.getPower());
		}

		String rezultatulMetodeiS = " ";

		for (MonomDouble temp : C.getP1()) {
			rezultatulMetodeiS = rezultatulMetodeiS + toString2(temp.getCoefficient(), temp.getPower());
		}
		
		
		String restulMeuS = " ";

		for (MonomDouble temp : restulMeu.getP1()) {
			restulMeuS = restulMeuS + toString2( temp.getCoefficient(), temp.getPower());
		}

		String restulMetodeiS = " ";

		for (MonomDouble temp : rest.getP1()) {
			restulMetodeiS = restulMetodeiS + toString2(temp.getCoefficient(), temp.getPower());
		}
		
		
		Assert.assertEquals(rezultatulMetodeiS, rezultatulMeuS);;
		Assert.assertEquals(restulMetodeiS, restulMeuS);

	}

	
	
}
