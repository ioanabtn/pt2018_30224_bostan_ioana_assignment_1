package controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Consumer;

import javax.swing.JOptionPane;

import model.Monom;
import model.MonomDouble;
import model.MonomInt;
import model.Polinom;
import view.MainFrame;

public class MainController {
	private MainFrame mainFrame;

	private Polinom polPD;
	private Polinom polQ;

	private Polinom rezPD;
	private Polinom rezQ;

	private Polinom rezAdunare;
	private Polinom rezScadere;
	private Polinom rezInmultire = new Polinom();
	private Polinom rezImpartire = new Polinom();
	private Polinom rest = new Polinom();

	private String polinom = " ";

	public String toString1(int c, int p) {
		return "+" + "(" + (int) (c) + "*x^" + p + ")";
	}

	public MainController() {
		this.mainFrame = new MainFrame();

		mainFrame.setVisible(true);

		ArrayList<MonomDouble> p = new ArrayList<MonomDouble>();
		ArrayList<MonomDouble> q = new ArrayList<MonomDouble>();

		DecimalFormat numberFormat = new DecimalFormat("0.00");

		Consumer<MonomDouble> cLambda1, cLambda2, cLambda3, cLambda4, cLambda5;

		cLambda1 = m2 -> mainFrame.getqTextField().setText(mainFrame.getqTextField().getText() + "+" + "("
				+ (int) (m2.getCoefficient()) + "*x^" + m2.getPower() + ")");
		cLambda2 = m3 -> mainFrame.getRezultatLabel().setText(mainFrame.getRezultatLabel().getText() + "+" + "("
				+ (int) (m3.getCoefficient()) + "*x^" + m3.getPower() + ")");
		cLambda3 = m4 -> mainFrame.getRezultatLabel().setText(mainFrame.getRezultatLabel().getText() + "+" + "("
				+ (int) (m4.getCoefficient()) + "*x^" + m4.getPower() + ")");
		cLambda4 = m5 -> mainFrame.getRezultatLabel().setText(mainFrame.getRezultatLabel().getText() + "+" + "("
				+ numberFormat.format(m5.getCoefficient()) + "*x^" + m5.getPower() + ")");
		cLambda5 = m6 -> mainFrame.getRestLabel().setText(
				mainFrame.getRestLabel().getText() + "+" + "(" + m6.getCoefficient() + "*x^" + m6.getPower() + ")");

		mainFrame.addAddPButtonActionListener(e -> {

			try {

				mainFrame.getpTextField().setText(" ");
				p.add(new MonomDouble(mainFrame.getCoefPTextField(), mainFrame.getPuterePTextField()));

				polPD = new Polinom(p);

				polinom = " ";

				for (MonomDouble temp : polPD.getP1()) {
					polinom = polinom + toString1((int) temp.getCoefficient(), temp.getPower());
				}

				mainFrame.getpTextField().setText(polinom);
				mainFrame.getpTextField().repaint();
				mainFrame.getpTextField().revalidate();
				mainFrame.getpTextField().setEditable(false);
				
				mainFrame.getCoefPTextField1().setText("");
				mainFrame.getPuterePTextField1().setText("");

				

			} catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Nu ati introdus un monom valid!", "Error",
						JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException eee) {
				JOptionPane.showMessageDialog(null, "Nu ati introdus un monom valid!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addDelPButtonActionListener(e -> {

			try {

				p.clear();
				rest.getP1().clear();
				mainFrame.getRestLabel().setText("");

				polinom = " ";
				mainFrame.getpTextField().setText(polinom);
				mainFrame.getpTextField().repaint();
				mainFrame.getpTextField().revalidate();
				mainFrame.getRezultatLabel().setText("");

			} catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de sters?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException eee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de sters?", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addAddQButtonActionListener(e -> {

			try {
				q.add(new MonomDouble(mainFrame.getCoefQTextField(), mainFrame.getPutereQTextField()));

				mainFrame.getqTextField().setText(" ");
				mainFrame.getqTextField().repaint();
				mainFrame.getqTextField().revalidate();
				mainFrame.getqTextField().setEditable(false);

				polQ = new Polinom(q);
				polQ.getP1().forEach(cLambda1);
		
				mainFrame.getCoefQTextField1().setText("");
				mainFrame.getPutereQTextField1().setText("");

			} catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Nu ati introdus un monom valid!", "Error",
						JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException eee) {
				JOptionPane.showMessageDialog(null, "Nu ati introdus un monom valid!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addDelQButtonActionListener(e -> {
			try {
				q.clear();
				rest.getP1().clear();
				mainFrame.getqTextField().setText(" ");
				mainFrame.getqTextField().repaint();
				mainFrame.getqTextField().revalidate();
				mainFrame.getRezultatLabel().setText("");
				mainFrame.getRestLabel().setText("");

			} catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de sters?", "Error",
						JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException eee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de sters?", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addDerivarePButtonActionListener(e -> {

			try{
	
			
			rezPD = polPD.Derivare();

			mainFrame.getRezultatLabel().setText(" ");

			rezPD.getP1().forEach(cLambda2);
			}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de derivat?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addIntegrarePButtonActionListener(e -> {

			try{
				p.clear();
				rezPD = polPD.Integrare();

			mainFrame.getRezultatLabel().setText(" ");
			for (MonomDouble temp : rezPD.getP1()) {

				mainFrame.getRezultatLabel().setText(mainFrame.getRezultatLabel().getText() + "+" + "("
						+ numberFormat.format(temp.getCoefficient()) + "*x^" + temp.getPower() + ")");
			}}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de integrat?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

			

		});

		mainFrame.addDerivareQButtonActionListener(e -> {

			try
			{rezQ = polQ.Derivare();

			mainFrame.getRezultatLabel().setText(" ");

			rezQ.getP1().forEach(cLambda2);
			}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de derivat?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addIntegrareQButtonActionListener(e -> {
			try {
			rezQ = polQ.Integrare();
			mainFrame.getRezultatLabel().setText(" ");

			for (MonomDouble temp : rezQ.getP1()) {

				mainFrame.getRezultatLabel().setText(mainFrame.getRezultatLabel().getText() + "+" + "("
						+ numberFormat.format(temp.getCoefficient()) + "*x^" + temp.getPower() + ")");

			}}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de integrat?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}


		});

		mainFrame.addAdunareButtonActionListener(e -> {

			try{rezAdunare = polPD.Adunare(polQ);

			mainFrame.getRezultatLabel().setText(" ");
			rezAdunare.getP1().forEach(cLambda3);
			}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de adunat?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}


		});

		mainFrame.addScadereButtonActionListener(e -> {
			

			try{rezScadere = polPD.Scadere(polQ);

			mainFrame.getRezultatLabel().setText(" ");
			rezScadere.getP1().forEach(cLambda3);
			}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de scazut?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		});

		mainFrame.addInmultireButtonActionListener(e -> {
			try {
			rezInmultire.getP1().clear();

			rezInmultire = rezInmultire.Inmultire(polPD.getP1(), polQ.getP1());

			mainFrame.getRezultatLabel().setText(" ");
			rezInmultire.getP1().forEach(cLambda3);
			}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de inmultit?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}


		});

		mainFrame.addImpartireButtonActionListener(e -> {
			
			try {

			if (polPD.maxim(polPD.getP1()).getPower() >= polQ.maxim(polQ.getP1()).getPower()) {
				rest.getP1().clear();
				rezImpartire = polPD.Impartire(polQ, rest);

				mainFrame.getRezultatLabel().setText(" ");
				mainFrame.getRestLabel().setText(" ");

				for (int i = 0; i < rezImpartire.getP1().size(); i++) {
					if (rezImpartire.getP1().get(i).getCoefficient() == 0)
						rezImpartire.getP1().remove(i);

				}
				for (int i = 0; i < rest.getP1().size(); i++) {
					if (rest.getP1().get(i).getCoefficient() == 0)
						rest.getP1().remove(i);

				}

				rezImpartire.getP1().forEach(cLambda4);
				rest.getP1().forEach(cLambda5);

			} else {
				JOptionPane.showMessageDialog(null, "Schimbati ordinea polinoamelor!", "Error",
						JOptionPane.ERROR_MESSAGE);
				mainFrame.getRezultatLabel().setText(" ");
				mainFrame.getRestLabel().setText(" ");
			}
			}catch (NullPointerException ee) {
				JOptionPane.showMessageDialog(null, "Sunteti sigur ca aveti ceva de impartit?!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}


		});
	}

}
