package model;

public class MonomDouble extends Monom implements Comparable<MonomDouble> {

	private double coefficient;

	public MonomDouble(double coef, int pow) {
		super(pow);
		this.coefficient = coef;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}

	@Override
	public int compareTo(MonomDouble m) {
		return -this.getPower() + m.getPower();
	}

}
