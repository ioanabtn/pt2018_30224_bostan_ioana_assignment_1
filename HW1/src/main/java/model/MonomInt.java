package model;

public class MonomInt extends Monom {

	private int coefficient;

	public MonomInt(int coef, int pow) {
		super(pow);
		this.coefficient = coef;
	}

	public int getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}

}
