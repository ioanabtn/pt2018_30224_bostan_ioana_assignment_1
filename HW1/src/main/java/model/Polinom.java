package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Consumer;

public class Polinom {

	private ArrayList<MonomDouble> p1 = new ArrayList<MonomDouble>();

	public Polinom() {};

	public Polinom(ArrayList<MonomDouble> l) {
		for (MonomDouble temp : l) {
			this.p1.add(temp);
		}
	}

	public void setP(ArrayList<MonomDouble> p) {
		this.p1 = p;
	}

	public ArrayList<MonomDouble> getP1() {
		return p1;
	}

	public Polinom Derivare() {
		
		for(int i = 0; i< this.p1.size();  i++) {
			if (this.p1.get(i).getPower()== 0)
				this.p1.remove(i);
		}

		for (MonomDouble temp : this.p1) {
			temp.setCoefficient(temp.getCoefficient() * temp.getPower());
			temp.setPower(temp.getPower() - 1);
		}
		
		Collections.sort(this.p1);
		
		return this;

	}

	public Polinom Integrare() {
		for (MonomDouble temp : this.p1) {
			temp.setCoefficient((temp.getCoefficient() / (temp.getPower() + 1)));
			temp.setPower(temp.getPower() + 1);
		}
		
		Collections.sort(this.p1);
		
		return this;
	}

	public Polinom Adunare(Polinom P) {
		
		Polinom aux = new Polinom();
		
		aux.getP1().addAll(P.getP1());
		aux.getP1().addAll(this.p1);
		
		for (int i = 0; i < aux.getP1().size() - 1; i++)
			for (int j = i + 1; j < aux.getP1().size(); j++) {
				if (aux.getP1().get(i).getPower() == aux.getP1().get(j).getPower()) {
					aux.getP1().get(i)
							.setCoefficient(aux.getP1().get(i).getCoefficient() + aux.getP1().get(j).getCoefficient());
					aux.getP1().remove(j);
				}
			}
		
		Collections.sort(aux.getP1());
		
		return aux;
	}

	public Polinom Scadere(Polinom P) {
		
		for (MonomDouble temp : P.getP1()) {
			temp.setCoefficient(temp.getCoefficient()*(-1));
		}
		Polinom aux = new Polinom();
		
		
		aux=this.Adunare(P);
		
		for(int i = 0; i< aux.getP1().size();  i++) {
			if (aux.getP1().get(i).getCoefficient()== 0)
				aux.getP1().remove(i);
			
		}
		Collections.sort(aux.getP1());
		
		return aux;
	}

	public Polinom Inmultire(ArrayList<MonomDouble> P, ArrayList<MonomDouble> Q) {

		MonomDouble aux;

		for (MonomDouble temp : P) {

			for (MonomDouble temp1 : Q) {
				aux = new MonomDouble(temp1.getCoefficient() * temp.getCoefficient(),
						temp1.getPower() + temp.getPower());
				this.p1.add(aux);
			}
			for (int i = 0; i < this.p1.size() - 1; i++)
				for (int j = i + 1; j < this.p1.size(); j++) {
					if (this.p1.get(i).getPower() == this.p1.get(j).getPower()) {
						this.p1.get(i)
								.setCoefficient(this.p1.get(i).getCoefficient() + this.p1.get(j).getCoefficient());
						this.p1.remove(j);
					}
				}
		}
		
		Collections.sort(this.p1);

		return this;
	}
	
	public void afisare(Polinom P) {
		for(MonomDouble temp: P.getP1()) {
			System.out.println(temp.getCoefficient()+ "x^"+temp.getPower());
		}
	}

	public MonomDouble maxim(ArrayList<MonomDouble> l) {
		MonomDouble max = new MonomDouble(0,0);
		for( MonomDouble temp: l) {
			if(temp.getPower()>max.getPower())
				max=temp;
		}
		return max;
	}
	
	
	
	public Polinom Impartire(Polinom I, Polinom R) { //I-Impartitor, R-cat
		Collections.sort(I.getP1());
		Polinom D = new Polinom(); //deimpartit
		D.getP1().addAll(this.p1);
		Collections.sort(D.getP1());
		Polinom C = new Polinom();
		while(D.maxim(D.getP1()).getPower() >I.maxim(I.getP1()).getPower()) {
			MonomDouble m1 = D.maxim(D.getP1());
			MonomDouble m2 = I.maxim(I.getP1());
			MonomDouble m3 = new MonomDouble(m1.getCoefficient()/m2.getCoefficient(), m1.getPower()-m2.getPower());
			C.getP1().add(m3);
			Polinom maux = new Polinom();
			maux.getP1().add(m3);
			Polinom aux = new Polinom();
			aux.Inmultire(I.getP1(), maux.getP1());
			D = D.Scadere(aux);
		}
		MonomDouble m1=D.maxim(D.getP1());
		MonomDouble m2 = I.maxim(I.getP1());
		MonomDouble m3 = new MonomDouble(m1.getCoefficient()/m2.getCoefficient(), m1.getPower()-m2.getPower());
		C.getP1().add(m3);
		Polinom maux = new Polinom();
		maux.getP1().add(m3);
		Polinom aux = new Polinom();
		aux.Inmultire(I.getP1(), maux.getP1());
		D = D.Scadere(aux);
		R.getP1().addAll(D.getP1());
		return C;
	}
}
