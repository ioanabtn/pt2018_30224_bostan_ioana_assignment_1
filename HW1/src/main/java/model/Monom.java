package model;

public class Monom {

	private int power;
	private int coeficient;

	public Monom(int pow) {
		this.power = pow;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

}
