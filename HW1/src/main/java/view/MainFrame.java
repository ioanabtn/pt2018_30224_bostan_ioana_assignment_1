package view;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;


public class MainFrame extends JFrame {

	
	private JTextField coefPTextField;
	private JTextField puterePTextField;
	private JTextField coefQTextField;
	private JTextField putereQTextField;
	private JButton AddPButton;
	private JButton AddQButton;
	private JLabel RezultatLabel;
	private JButton AdunareButton;
	private JButton ScadereButton;
	private JButton DerivarePButton;
	private JButton DerivareQButton;
	private JButton IntegrarePButton;
	private JButton IntegrareQButton;
	private JButton DelPButton;
	private JButton DelQButton;
	private JTextField pTextField;
	private JTextField qTextField;
	private JButton InmultireButton;
	private JButton ImpartireButton;
	private JLabel restLabel;
	
	
	
	public MainFrame() {
		
		
		this.setTitle("Operatii cu Polinoame");
		this.setBounds(100, 100, 515, 301);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null); 
		
		JLabel PLabel = new JLabel("P:");
		PLabel.setBounds(10, 32, 25, 14);
		getContentPane().add(PLabel);
		
		JLabel QLabel = new JLabel("Q:");
		QLabel.setBounds(10, 68, 25, 14);
		getContentPane().add(QLabel);
		
		JLabel CoeficientLabel = new JLabel("Coeficient");
		CoeficientLabel.setBounds(47, 11, 66, 14);
		getContentPane().add(CoeficientLabel);
		
		JLabel PutereLabel = new JLabel("Putere");
		PutereLabel.setBounds(152, 11, 46, 14);
		getContentPane().add(PutereLabel);
		
		coefPTextField =  new JTextField();
		coefPTextField.setColumns(10);
		coefPTextField.setBounds(32, 29, 86, 20);
		getContentPane().add(coefPTextField);
		
		puterePTextField = new JTextField();
		puterePTextField.setBounds(128, 29, 86, 20);
		getContentPane().add(puterePTextField);
		puterePTextField.setColumns(10);
		
		coefQTextField = new JTextField();
		coefQTextField.setColumns(10);
		coefQTextField.setBounds(32, 65, 86, 20);
		getContentPane().add(coefQTextField);
		
		putereQTextField = new JTextField();
		putereQTextField.setColumns(10);
		putereQTextField.setBounds(128, 65, 86, 20);
		getContentPane().add(putereQTextField);
		
		AddPButton = new JButton("Add P");
		AddPButton.setBounds(246, 28, 108, 23);
		getContentPane().add(AddPButton);
		
		AddQButton = new JButton("Add Q");
		AddQButton.setBounds(246, 64, 108, 23);
		getContentPane().add(AddQButton);
		
		JLabel RezultatLabel1 = new JLabel("REZULTAT:");
		RezultatLabel1.setBounds(10, 235, 76, 14);
		getContentPane().add(RezultatLabel1);
		
		RezultatLabel = new JLabel("");
		RezultatLabel.setBounds(73, 235, 389, 14);
		getContentPane().add(RezultatLabel);
		
		AdunareButton = new JButton("Adunare");
		AdunareButton.setBounds(20, 96, 98, 23);
		getContentPane().add(AdunareButton);
		
		ScadereButton = new JButton("Scadere");
		ScadereButton.setBounds(20, 130, 98, 23);
		getContentPane().add(ScadereButton);
		
		DerivarePButton = new JButton("Derivare P");
		DerivarePButton.setBounds(128, 130, 105, 23);
		getContentPane().add(DerivarePButton);
		
		DerivareQButton = new JButton("Derivare Q");
		DerivareQButton.setBounds(246, 130, 108, 23);
		getContentPane().add(DerivareQButton);
		
		IntegrarePButton = new JButton("Integrare P");
		IntegrarePButton.setBounds(128, 98, 105, 23);
		getContentPane().add(IntegrarePButton);
		
		IntegrareQButton = new JButton("Intergrare Q");
		IntegrareQButton.setBounds(246, 98, 108, 23);
		getContentPane().add(IntegrareQButton);
		
		JLabel PLabel1 = new JLabel("P");
		PLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		PLabel1.setBounds(10, 175, 35, 14);
		getContentPane().add(PLabel1);
		
		JLabel QLabel1 = new JLabel("Q");
		QLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		QLabel1.setBounds(10, 210, 35, 14);
		getContentPane().add(QLabel1);
		
		DelPButton = new JButton("DelP");
		DelPButton.setBounds(373, 28, 89, 23);
		getContentPane().add(DelPButton);
		
		DelQButton = new JButton("DelQ");
		DelQButton.setBounds(373, 64, 89, 23);
		getContentPane().add(DelQButton);
		
		pTextField = new JTextField();
		pTextField.setBounds(46, 172, 308, 20);
		getContentPane().add(pTextField);
		pTextField.setColumns(10);
		
		qTextField = new JTextField();
		qTextField.setColumns(10);
		qTextField.setBounds(46, 207, 308, 20);
		getContentPane().add(qTextField);
		
		InmultireButton = new JButton("Inmultire");
		InmultireButton.setBounds(373, 98, 89, 23);
		getContentPane().add(InmultireButton);
		
		ImpartireButton = new JButton("Impartire");
		ImpartireButton.setBounds(373, 130, 89, 23);
		getContentPane().add(ImpartireButton);
		
		JLabel RLabel = new JLabel("Rest:");
		RLabel.setBounds(373, 175, 54, 14);
		getContentPane().add(RLabel);
		
	    restLabel = new JLabel("");
		restLabel.setBounds(373, 210, 116, 14);
		getContentPane().add(restLabel);
	}
	public JTextField getpTextField() {
		return pTextField;
	}
	public void setpTextField(JTextField pTextField) {
		this.pTextField = pTextField;
	}
	public JTextField getqTextField() {
		return qTextField;
	}
	public void setqTextField(JTextField qTextField) {
		this.qTextField = qTextField;
	}
	public int getCoefPTextField() {
		return Integer.parseInt(coefPTextField.getText());
	}
	public JTextField getCoefPTextField1() {
		return coefPTextField;
	}
	public void setCoefPTextField(JTextField coefPTextField) {
		this.coefPTextField = coefPTextField;
	}
	public JTextField getPuterePTextField1() {
		return puterePTextField;
	}
	public int getPuterePTextField() {
		return Integer.parseInt(puterePTextField.getText());
	}
	
	
	public void setPuterePTextField(JTextField puterePTextField) {
		this.puterePTextField = puterePTextField;
	}
	public int getCoefQTextField() {
		return Integer.parseInt(coefQTextField.getText());
	}
	public JTextField getCoefQTextField1() {
		return coefQTextField;
	}
	public void setCoefQTextField(JTextField coefQTextField) {
		this.coefQTextField = coefQTextField;
	}
	public int getPutereQTextField() {
		return Integer.parseInt(putereQTextField.getText());
	}
	public JTextField getPutereQTextField1(){
		return putereQTextField;
	}
	public void setPutereQTextField(JTextField putereQTextField) {
		this.putereQTextField = putereQTextField;
	}
	public JLabel getRezultatLabel() {
		return RezultatLabel;
	}
	public void setRezultatLabel(JLabel rezultatLabel) {
		RezultatLabel = rezultatLabel;
	}
	
	public JLabel getRestLabel() {
		return restLabel;
	}
	public void setResttLabel(JLabel restLabel) {
		restLabel = restLabel;
	}
	
	
	public void addAddPButtonActionListener(ActionListener al) {
		this.AddPButton.addActionListener(al);
	}
	
	public void addAddQButtonActionListener(ActionListener al) {
		this.AddQButton.addActionListener(al);
	}
	public void addAdunareButtonActionListener(ActionListener al) {
		this.AdunareButton.addActionListener(al);
	}
	public void addScadereButtonActionListener(ActionListener al) {
		this.ScadereButton.addActionListener(al);
	}
	public void addDerivarePButtonActionListener(ActionListener al) {
		this.DerivarePButton.addActionListener(al);
	}
	public void addDerivareQButtonActionListener(ActionListener al) {
		this.DerivareQButton.addActionListener(al);
	}
	public void addIntegrarePButtonActionListener(ActionListener al) {
		this.IntegrarePButton.addActionListener(al);
	}
	public void addIntegrareQButtonActionListener(ActionListener al) {
		this.IntegrareQButton.addActionListener(al);
	}
	public void addDelPButtonActionListener(ActionListener al) {
		this.DelPButton.addActionListener(al);
	}
	public void addDelQButtonActionListener(ActionListener al) {
		this.DelQButton.addActionListener(al);
	}
	public void addInmultireButtonActionListener(ActionListener al) {
		this.InmultireButton.addActionListener(al);
	}
	public void addImpartireButtonActionListener(ActionListener al) {
		this.ImpartireButton.addActionListener(al);
	}
}
